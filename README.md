# Source Content Pack

Разнообразные игровые ресурсы, собранные мной в один репозиторий.
Обратите внимание, что ресурсы были переименованы для поддержания единого стиля именования.

My personal asset collection.
Notice that assets were renamed to follow consitstent naming style

# Содержимое / Pack contents

| Название | Name | Ссылка/Link | Автор(ы)/Author(s) |
| -------- | ---- | ----------- | ------------------ |
| Ресурсы мода CUBE | CUBE modification assets | https://www.moddb.com/mods/cube-mod | AASC |
| Ресурсы мода Hatch-18 | Hatch-18 modification assets | https://www.moddb.com/mods/hatch-18 | AASC |
| Текстуры утекшей версии Half-Life 2 ("Beta") | Half-Life 2 Leak textures | | Valve |
| Текстуры и некоторые модели из MegaPatch v4.1 | MegaPatch v4.1 textures and some models | https://hl2-beta.ru/index.php?action=downloads;sa=view;down=34 | |
| Текстуры из 2001-2002 Leak Texture Pack | 2001-2002 Leak Texture Pack textures | https://hl2-beta.ru/index.php?action=downloads;sa=view;down=301 | Klow, Hgrunt, Bun, Shift |
| Текстуры с Half-Life Discord Community | Half-Life Discord Community textures | | <Забыл / Fogotten> |
| Текстуры и модели из ZONA Stalker Props Pack | ZONA Stalker Props Pack textures and models | https://steamcommunity.com/sharedfiles/filedetails/?id=430077474 | |
| Текстуры из утечки репозитория 2023 года | The 2023 repository leak textures | | Valve, recompiled by Ellent |
| Модели из Half-Life 2 Leak Props: Complete | Models from Half-Life 2 Leak Props: Complete | https://steamcommunity.com/sharedfiles/filedetails/?id=310835919 | CrazyBubba |
| Ресурсы из The Stanley Parable | The Stanley Parable assets | | |
| Текстуры из Portal 1 | Portal 1 textures | | Valve |
| Ресурсы мода Snowdrop Escape | Snowdrop Escape mod assets | https://www.moddb.com/mods/sde | SDE dev team |
| <Ресурсы из нескольких карт для Garry's mod> | <Assets from some Garry's mod maps> | <Забыл / Fogotten> | <Забыл / Fogotten> |
| Ресурсы из Dark Interval | Dark Interval assets | https://www.moddb.com/mods/dark-interval | <TODO> |
| City8 | City8 | <TODO> | <TODO> |
| rp_zaton_remastered | rp_zaton_remastered | <TODO> | <TODO> |
| Зной | Swelter | https://www.moddb.com/mods/swelter | SDE dev team |
| gm_boreas | gm_boreas | https://steamcommunity.com/sharedfiles/filedetails/?id=1572373847 | https://steamcommunity.com/id/Blueberry_pie |

Various other assets by:
- Cryptical